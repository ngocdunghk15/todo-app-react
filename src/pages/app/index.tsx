import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import {Button} from "@/components/ui/button"
import {
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from "@/components/ui/card"
import {Input} from "@/components/ui/input"
import {Separator} from "@/components/ui/separator";
import {useForm} from "react-hook-form";
import {z} from "zod";
import {zodResolver} from "@hookform/resolvers/zod";
import {useState} from "react";
import {v4 as uuid} from "uuid";

const createFormSchema = z.object({
  name: z.string().min(1, {message: "Task name is required!"})
})


function AppPage() {
  const [todos, setTodos] = useState<any>([]);
  const createForm = useForm<z.infer<typeof createFormSchema>>({
    resolver: zodResolver(createFormSchema),
  })

  function onCreateTask(values: z.infer<typeof createFormSchema>) {
    // Do something with the form values.
    setTodos((todos: any) => (
      [
        ...todos,
        {
          id: uuid(),
          name: values?.name
        }
      ]
    ))
    createForm.reset();
  }

  return <div className={"h-lvh flex flex-col justify-center items-center"}>
    <h1 className={"text-3xl font-semibold mb-8"}>Todo app</h1>
    <Card className="mx-auto max-w-lg">
      <CardHeader>
        <CardTitle className="text-xl">Add new task</CardTitle>
        <CardDescription>
          Enter your information to create tasks
        </CardDescription>
      </CardHeader>
      <CardContent>
        <Form {...createForm}>
          <form onSubmit={createForm.handleSubmit(onCreateTask)}>
            <FormField
              control={createForm.control}
              name="name"
              render={({field}) => (
                <FormItem>
                  <FormLabel>Name</FormLabel>
                  <div className={'flex gap-2 items-center'}>
                    <FormControl>
                      <Input
                        {...field}
                        placeholder="Something to do..."
                      />
                    </FormControl>
                    <Button type={'submit'} className={'gap-1'}>
                      Add
                    </Button>
                  </div>
                  <FormMessage/>
                </FormItem>
              )}
            />
          </form>
        </Form>
      </CardContent>
      <div className={'px-6'}>
        <Separator className={'my-0'}/>
      </div>
      <CardHeader>
        <CardTitle className="text-xl">Todo lists</CardTitle>
        <CardDescription>
          Manage your todo lists
        </CardDescription>
      </CardHeader>
      <CardContent>
        <div className="grid gap-2">
          {!!todos.length ? todos.map((todo: any) => (
            <div className={"flex justify-between"}>
              <span className={"block"}>
                {todo?.name}
              </span>
              <div>
                <span className={"text-red-500 cursor-pointer font-semibold underline"} onClick={() => {
                  setTodos((todosz: any) => (todosz.filter((todoz: any) =>{
                    return  todoz.id !== todo.id
                  })))
                }}>
                  Remove
                </span>
              </div>
            </div>
          )) : <div>
            Have not tasks yet.
          </div>}
        </div>
      </CardContent>
    </Card>
  </div>
}

export default AppPage;