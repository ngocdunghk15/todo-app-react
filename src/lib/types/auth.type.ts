export interface Account {
  avatar: string;
  createdAt: Date;
  displayName: string;
  email: string;
  isActivated: boolean;
  roles: string[];
  updatedAt: Date;
  username: string;
  _id: string;
}

export interface DoLoginPayload {
  loginField: string;
  password: string;
}

export interface DoLoginReturned {
  access_token: string;
  refresh_token: string;
  account: Account;
}

export interface DoRegisterPayload {
  username: string;
  email: string;
  password: string;
  confirmPassword?: string;
}

export interface GetAccountInfoReturned {
  data: {
    account: Account;
  };
  success: boolean;
}

export interface LoginWithSSOPayload {
  redirectUri: string;
  authorizationCode: string;
  service: string;
}
