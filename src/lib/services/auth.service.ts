import {httpService} from "@/lib/services/http.service.ts";
import {DoLoginPayload, DoLoginReturned, DoRegisterPayload, LoginWithSSOPayload} from "@/lib/types/auth.type.ts";

class AuthService {
  public login = async (payload: DoLoginPayload) => {
    return await httpService.post<DoLoginPayload, DoLoginReturned>('/auth/login', payload, { isPublicApi: true });
  };

  public loginWithSSO = async (payload: LoginWithSSOPayload) => {
    return await httpService.post<LoginWithSSOPayload, any>('/auth/sso', payload, { isPublicApi: true });
  };

  public register = async (payload: DoRegisterPayload) => {
    return await httpService.post<DoRegisterPayload, unknown>('/auth/register', payload, { isPublicApi: true });
  };

  public getAccountInfo = async () => {
    return await httpService.get<any>('/accounts/info');
  };
}

export const authService = new AuthService();
