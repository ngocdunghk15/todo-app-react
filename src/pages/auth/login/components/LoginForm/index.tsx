import {cn} from "@/lib/utils"
import {Button} from "@/components/ui/button"
import {Input} from "@/components/ui/input";
import {useForm} from "react-hook-form";
import {z} from "zod";
import {zodResolver} from "@hookform/resolvers/zod";
import {Form, FormControl, FormField, FormItem, FormLabel} from "@/components/ui/form";
import React from "react";
import {useSnackbar} from "notistack";
import {authService} from "@/lib/services/auth.service.ts";
import StorageService from "@/lib/services/storage.service.ts";
import {Token} from "@/lib/enums/app.enum.ts";
import {useNavigate} from "react-router-dom";

interface AuthFormProps extends React.HTMLAttributes<HTMLDivElement> {
}

const FormSchema = z.object({
  loginField: z.string().min(1, {
    message: "Please enter username or password.",
  }),
  password: z.string().min(8)
})

function LoginForm({className, ...props}: AuthFormProps) {
  const navigate = useNavigate();
  const {enqueueSnackbar} = useSnackbar();
  const form = useForm<z.infer<typeof FormSchema>>({
    resolver: zodResolver(FormSchema),
  })

  async function onSubmit(payload: z.infer<typeof FormSchema>) {
    try {
      const response = (await authService.login(payload))?.data;
      StorageService.set(Token.ACCESS, response?.access_token);
      StorageService.set(Token.REFRESH, response?.refresh_token);
      navigate('/app');
    } catch {
      enqueueSnackbar('Username or password is not connect!', { variant: 'error' });
    }
  }

  return <div className={cn("grid gap-6", className)} {...props}>
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)}>
        <div className="grid gap-2">
          <FormField
            control={form.control}
            name="loginField"
            render={({field}) => (
              <FormItem>
                <FormLabel>Username or email</FormLabel>
                <FormControl>
                  <Input placeholder="Username or email..." {...field} />
                </FormControl>
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="password"
            render={({field}) => (
              <FormItem>
                <FormLabel>Password</FormLabel>
                <FormControl>
                  <Input type={"password"} placeholder="Password..." {...field} />
                </FormControl>
              </FormItem>
            )}
          />
          <Button className={"mt-3"} type="submit">
            Login
          </Button>
        </div>
      </form>
    </Form>

    <div className="relative">
      <div className="absolute inset-0 flex items-center">
        <span className="w-full border-t"/>
      </div>
      <div className="relative flex justify-center text-xs uppercase">
          <span className="bg-background px-2 text-muted-foreground">
            Or continue with
          </span>
      </div>
    </div>
    <Button variant="outline" type="button" disabled={true}>
      GitHub
    </Button>
  </div>
}

export default LoginForm;