import {CreateAxiosConfigs} from "@/lib/types/http.type.ts";
import {VITE_API_ENDPOINT} from "@/lib/configs/env.config.ts";

export const axiosConfigs: CreateAxiosConfigs = {
  baseURL: VITE_API_ENDPOINT,
  timeout: 15000
};
