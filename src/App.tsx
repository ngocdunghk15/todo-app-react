import './App.css'
import {BrowserRouter, Navigate, Route, Routes} from "react-router-dom";
import AppPage from "@/pages/app";
import LoginPage from "@/pages/auth/login";
import RegisterPage from "@/pages/auth/register";
import {SnackbarProvider} from "notistack";

function App() {
  return (
    <SnackbarProvider>
      <BrowserRouter>
        <Routes>
          <Route path={"/"} element={<Navigate to={"/auth/login"}/>} />
          <Route path={"/app"} element={<AppPage/>}/>
          <Route path={"/auth"}>
            <Route path={"login"} element={<LoginPage/>}/>
            <Route path={"register"} element={<RegisterPage/>}/>
          </Route>
        </Routes>
      </BrowserRouter>
    </SnackbarProvider>
  )
}

export default App
