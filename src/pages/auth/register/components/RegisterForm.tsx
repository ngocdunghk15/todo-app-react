import {cn} from "@/lib/utils"
import {Button} from "@/components/ui/button"
import {Input} from "@/components/ui/input";
import {useForm} from "react-hook-form";
import {z} from "zod";
import {zodResolver} from "@hookform/resolvers/zod";
import {Form, FormControl, FormField, FormItem, FormLabel} from "@/components/ui/form";
import React from "react";
import {useSnackbar} from "notistack";
import {authService} from "@/lib/services/auth.service.ts";
import {useNavigate} from "react-router-dom";

interface AuthFormProps extends React.HTMLAttributes<HTMLDivElement> {
}

const FormSchema = z.object({
  username: z.string().min(1, {
    message: "Please enter username.",
  }),
  email: z.string().min(1, {
    message: "Please enter username.",
  }).email("Please enter valid email!"),
  password: z.string().min(8)
})

function RegisterForm({className, ...props}: AuthFormProps) {
  const navigate = useNavigate();
  const {enqueueSnackbar} = useSnackbar();
  const form = useForm<z.infer<typeof FormSchema>>({
    resolver: zodResolver(FormSchema),
  })

  async function onSubmit(payload: z.infer<typeof FormSchema>) {
    try {
      await authService.register({
        ...payload,
        confirmPassword: payload?.password
      });
      enqueueSnackbar('Register account successfully!', { variant: 'success' });
      navigate('/auth/login');
    } catch {
      enqueueSnackbar('Failed to register account!', {variant: 'error'});
    }
  }

  return <div className={cn("grid gap-6", className)} {...props}>
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)}>
        <div className="grid gap-2">
          <FormField
            control={form.control}
            name="username"
            render={({field}) => (
              <FormItem>
                <FormLabel>Username</FormLabel>
                <FormControl>
                  <Input placeholder="Username..." {...field} />
                </FormControl>
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="email"
            render={({field}) => (
              <FormItem>
                <FormLabel>Email</FormLabel>
                <FormControl>
                  <Input placeholder="Email..." {...field} />
                </FormControl>
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="password"
            render={({field}) => (
              <FormItem>
                <FormLabel>Password</FormLabel>
                <FormControl>
                  <Input type={"password"} placeholder="Password..." {...field} />
                </FormControl>
              </FormItem>
            )}
          />
          <Button className={"mt-3"} type="submit">
            Register
          </Button>
        </div>
      </form>
    </Form>

    <div className="relative">
      <div className="absolute inset-0 flex items-center">
        <span className="w-full border-t"/>
      </div>
      <div className="relative flex justify-center text-xs uppercase">
          <span className="bg-background px-2 text-muted-foreground">
            Or continue with
          </span>
      </div>
    </div>
    <Button variant="outline" type="button" disabled={true}>
      GitHub
    </Button>
  </div>
}

export default RegisterForm;